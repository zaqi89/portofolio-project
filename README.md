This is my repository specifically for my personal projects using python and jupyter notebook in its execution.

1. Car Prediction Price -  Understanding a dataset by performing data overview, data preprocessing, data visualization, exploratory data analysis to develop multiple linear regression, decision tree regression and random forest regression machine learning models, then comparing the modeling results to each other to predict car prices based on the collected data.

2. Customers Segmentation: K-Means clustering is used for customer segmentation as a machine learning model, by visualizing data and looking at the overall customer data to identify behavior in spending habits to make clusters of customers.

3. Credit Score Classification - Credit score classification process using LogisticRegression, Decision Tree, Random Forest, Naive Bayes algorithms with data cleaning and missing value handling steps.

4. Sentiment analysis Using LSTM - Processing and storing data using tools such as tokenization, stemming, and stopword removal, after the data is processed, building an LSTM model; to do this, determining the model parameters and including the number of layers; evaluating the LSTM model using metrics such as accuracy, precision, and recall.

5. Credit Risk Prediction - Data understanding to determine the significantly influential variables, data processing using descriptive analysis methods and the use of SMOTE methods on data that is indicated to be imbalanced. Modeling with the use of SVM (support vector machine) that can classify data into labels.
